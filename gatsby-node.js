const path = require('path')
const glob = require('glob')

exports.onCreateWebpackConfig = ({ actions, stage }) => {
	// enable sourcemaps on dev
	// https: //github.com/gatsbyjs/gatsby/issues/6278
	if (stage === 'develop') {
		actions.setWebpackConfig({
			devtool: 'cheap-module-source-map',
		})
	}

	actions.setWebpackConfig({
		resolve: {
			modules: [path.join(__dirname, 'src'), 'node_modules'],
			alias: {
				'~components': path.resolve(__dirname, 'src/components'),
				'~utils': path.resolve(__dirname, 'src/utils'),
			},
		},
	})
}



// let resolvers = [...glob.sync("app/schema/*/*/*.resolver.js"), ...glob.sync("app/schema/*/*/*/*.resolver.js"), ...glob.sync("app/schema/*/*/*/*/*.resolver.js")];
exports.createPages = async ({ graphql, actions, reporter }) => {
	const { createPage } = actions
	// Query for markdown nodes to use in creating pages.
	// const result = await graphql(
	// 	`
	//     {
	//       allMarkdownRemark(limit: 1000) {
	//         edges {
	//           node {
	//             frontmatter {
	//               path
	//             }
	//           }
	//         }
	//       }
	//     }
	//   `
	// )
	// Handle errors
	// if (result.errors) {
	// 	reporter.panicOnBuild(`Error while running GraphQL query.`)
	// 	return
	// }
	// Create pages for each markdown file.
	// const blogPostTemplate = path.resolve(`src/templates/blog-post.js`)

	let pages = glob.sync("./src/features/*/*.page.js")

	for (const page of pages) {
		// add resolvers to array
		// registerResolvers = [...registerResolvers, require("./" + resolver)];

		let pageName = page.split(".page.js").join("").split("/").filter(function (el) { return el.length != 0 })

		pageName = pageName[pageName.length - 1].toString()

		const pageRoute = page.split(`${pageName}.page.js`).join("");


		const pageComponent = path.resolve(page);
		const pageMeta = require(`${pageRoute}${pageName}.json`)


		console.log('pageMETA', pageMeta)
		createPage({
			path: pageMeta.slug,
			component: pageComponent,
			context: {
				...pageMeta
			}
			// In your blog post template's graphql query, you can use pagePath
			// as a GraphQL variable to query for data from the markdown file.
			// context: {
			// 	pagePath: path,
			// },
		})
	}

}