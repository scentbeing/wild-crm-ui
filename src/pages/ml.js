import React from 'react'
import PropTypes from 'prop-types'
import Layout from '../layouts/AdminLayout'
import Content from '../layouts/AdminLayout/Content'

function MLPage({ location }) {
	const pageTitle = location ? location.pathname.replace(/\//g, '') : ''
	return (
		<Layout location={location} title={pageTitle}>
			<Content />
		</Layout>
	)
}
MLPage.propTypes = {
	location: PropTypes.object,
}
export default MLPage
