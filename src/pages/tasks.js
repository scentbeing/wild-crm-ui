import React from 'react'
import PropTypes from 'prop-types'
import Layout from '../layouts/AdminLayout'
import Content from '../layouts/AdminLayout/Content'

function AnalyticsPage({ data, location }) {
	const pageTitle = location ? location.pathname.replace(/\//g, '') : ''
	return (
		<Layout location={location} title={pageTitle}>
			<Content />
		</Layout>
	)
}
AnalyticsPage.propTypes = {
	data: PropTypes.object.isRequired,
	location: PropTypes.object,
}
export default AnalyticsPage
