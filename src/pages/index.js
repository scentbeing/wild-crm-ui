import React from 'react'
import { graphql } from 'gatsby'
import PropTypes from 'prop-types'
import Layout from '../layouts/AdminLayout'
import Content from '../layouts/AdminLayout/Content'
import PrivateRoute from '../layouts/PrivateRoute'

function DashboardIndex({ data, location }) {
	console.log('data', data, location)
	const { title } = data.site.siteMetadata
	return (
		<Layout location={location} title={title}>
			<Content />
		</Layout>
	)
}
DashboardIndex.propTypes = {
	data: PropTypes.object.isRequired,
	location: PropTypes.object,
}
export const pageQuery = graphql`
	query {
		site {
			siteMetadata {
				title
			}
		}
	}
`
export default PrivateRoute({ component: DashboardIndex })
