import React from 'react'
import PropTypes from 'prop-types'
import Layout from '../layouts/AdminLayout'
import Content from '../layouts/AdminLayout/Content'
import PrivateRoute from '../layouts/PrivateRoute'

function PerformancePage(props) {
	console.log("props from calendar", props)
	const pageTitle = "test"
	return (
		<Layout {...props} title={pageTitle} tabs={[]}>
			<Content />
		</Layout>
	)
}
// PerformancePage.propTypes = {
// 	location: PropTypes.object,
// }
export default PrivateRoute({component: PerformancePage}) // PerformancePage
