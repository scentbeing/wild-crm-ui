import React from 'react'
import PropTypes from 'prop-types'
import Layout from '../layouts/AdminLayout'
import StorageContent from '../layouts/AdminLayout/StorageContent'
function StoragePage({ location }) {
	const pageTitle = location ? location.pathname.replace(/\//g, '') : ''
	return (
		<Layout location={location} title={pageTitle}>
			<StorageContent />
		</Layout>
	)
}
StoragePage.propTypes = {
	location: PropTypes.object,
}
export default StoragePage
