import React, { useState, useEffect } from 'react'
import { navigate } from "gatsby"
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Alert from '@material-ui/lab/Alert';

import AuthLayout from "../../layouts/AuthLayout"
import { signIn } from "./auth.store"
import { isEmailValid } from "./auth.validation"


function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
      <Link color="inherit" href="https://mui.com/">
        Your Website
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

const useStyles = makeStyles((theme) => ({
  root: {
    height: '100vh',
  },
  image: {
    backgroundImage: 'url(https://source.unsplash.com/random)',
    backgroundRepeat: 'no-repeat',
    backgroundColor:
      theme.palette.type === 'light' ? theme.palette.grey[50] : theme.palette.grey[900],
    backgroundSize: 'cover',
    backgroundPosition: 'center',
  },
  paper: {
    margin: theme.spacing(8, 4),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

function LoginPage({ location }) {
  const classes = useStyles();

  const [forgotPasswordLink, setForgotPasswordLink] = useState("/forgot-password")
  const [signupLink, setSignupLink] = useState("/sign-up")
  const [emailInput, setEmailInput] = useState("")
  const [passwordInput, setPasswordInput] = useState("")
  const [emailInputError, setEmailInputError] = useState(null)
  const [passwordInputError, setPasswordInputError] = useState(null)
  const [errorMessage, setErrorMessage] = useState(null)


  useEffect(() => {
    if (location.search) {
      setForgotPasswordLink(`${forgotPasswordLink}${location.search}`)
      setSignupLink(`${signupLink}${location.search}`)
    }
  }, [])

  const handleEmailInput = (event) => {
    setEmailInput(event.target.value)
  }

  const handlePasswordInput = (event) => {
    setPasswordInput(event.target.value)
  }

  const resetErrors = async () => {
    setEmailInputError(null)
    setPasswordInputError(null)
    setErrorMessage(null)
  }

  const handleSubmit = async (event) => {
    event.preventDefault();

    let hasErrors = false;
    await resetErrors()

    const emailValid = await isEmailValid(emailInput)
    if (!emailValid.result || emailInput.length === 0) {
      setEmailInputError("Please enter a value email address.")
      hasErrors = true
    }

    if (passwordInput.length === 0) {
      setPasswordInputError("Please enter a password.")
      hasErrors = true
    }

    if (hasErrors) {
      return;
    }

    console.log('sign in')

    const letsSignIn = await signIn({
      email: emailInput,
      password: passwordInput
    })

    if (!letsSignIn.result) {
      setErrorMessage(letsSignIn.message)
      return
    }

    // console.log('signInResult', signInResult)

    const urlParams = new URLSearchParams(location.search)
    if (urlParams.get("return_url")) {
      navigate(decodeURI(urlParams.get("return_url")))

    } else {

      navigate("/dashboard")
    }
  }

  return (
    <AuthLayout>
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Sign in
        </Typography>
        <form className={classes.form} noValidate onSubmit={handleSubmit}>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="email"
            label="Email Address"
            name="email"
            autoComplete="email"
            autoFocus
            value={emailInput}
            onChange={handleEmailInput}
            error={emailInputError && emailInputError.length > 0}
            helperText={emailInputError}
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password"
            label="Password"
            type="password"
            id="password"
            autoComplete="current-password"
            value={passwordInput}
            onChange={handlePasswordInput}
            error={passwordInputError && passwordInputError.length > 0}
            helperText={passwordInputError}
          />
          {/* <FormControlLabel
              control={<Checkbox value="remember" color="primary" />}
              label="Remember me"
            /> */}
          {errorMessage && (
            <Alert severity="error">
              {errorMessage}
            </Alert>
          )}
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            Sign In
          </Button>
        </form>
        <Grid container>
          <Grid item xs>
            <Link href={forgotPasswordLink} variant="body2">
              Forgot password?
            </Link>
          </Grid>
          <Grid item>
            <Link href={signupLink} variant="body2">
              {"Don't have an account? Sign Up"}
            </Link>
          </Grid>
        </Grid>
        <Box mt={5}>
          <Copyright />
        </Box>
      </div>
    </AuthLayout>
  )
}

export default LoginPage
