import React, { useState, useEffect } from 'react'
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import Alert from '@material-ui/lab/Alert';
import { makeStyles } from '@material-ui/core/styles';

import AuthLayout from "../../layouts/AuthLayout"
import { isEmailValid } from './auth.validation';
import { sendForgotPasswordRequest } from "./auth.store"

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
      <Link color="inherit" href="https://mui.com/">
        Your Website
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

const useStyles = makeStyles((theme) => ({
  root: {
    height: '100vh',
  },
  image: {
    backgroundImage: 'url(https://source.unsplash.com/random)',
    backgroundRepeat: 'no-repeat',
    backgroundColor:
      theme.palette.type === 'light' ? theme.palette.grey[50] : theme.palette.grey[900],
    backgroundSize: 'cover',
    backgroundPosition: 'center',
  },
  paper: {
    margin: theme.spacing(8, 4),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

function ForgotPasswordPage({ location }) {
  const classes = useStyles();

  const [signupLink, setSignupLink] = useState("/login")

  const [emailInput, setEmailInput] = useState("")
  const [emailInputError, setEmailInputError] = useState(null)
  const [errorMessage, setErrorMessage] = useState(null)
  const [confirmationMessage, setConfirmationMessage] = useState(null)

  useEffect(() => {
    if (location?.search) {
      setSignupLink(`${signupLink}${location.search}`)
    }
  }, [])

  const handleSubmit = async (event) => {
    event.preventDefault()

    let hasErrors = false;
    await resetFormErrors()

    const emailValid = await isEmailValid(emailInput)
    if (!emailValid.result || emailInput.length === 0) {
      setEmailInputError("Please enter a value email address.")
      hasErrors = true
    }

    if (hasErrors) {
      return;
    }

    const processResult = await sendForgotPasswordRequest({ email: emailInput })

    if (processResult?.forgotPassword?.result === false) {
      setEmailInputError(processResult.forgotPassword.message)
      return;
    } else {
      setConfirmationMessage(`An email has been set to '${emailInput}'`)
    }
  }

  const resetFormErrors = async () => {
    setEmailInputError(null)
    setErrorMessage(null)
    setConfirmationMessage(null)
  }

  const handleEmailInput = event => {
    setEmailInput(event.target.value)
  }

  return (
    <AuthLayout>
      <div className={classes.paper}>
        <Grid container>
          <Grid item xs>
            <Link href={signupLink} variant="body2">
              Go Back to Sign In
            </Link>
          </Grid>
          {/* <Grid item>
              <Link href="#" variant="body2">
                {"Don't have an account? Sign Up"}
              </Link>
            </Grid> */}
        </Grid>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Forgot Password
        </Typography>
        <form className={classes.form} noValidate onSubmit={handleSubmit}>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="email"
            label="Email Address"
            name="email"
            autoComplete="email"
            autoFocus
            value={emailInput}
            onChange={handleEmailInput}
            error={emailInputError && emailInputError.length > 0}
            helperText={emailInputError}
          />
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            Send Email
          </Button>
          {confirmationMessage && (
            <Alert severity="success">{confirmationMessage}</Alert>)
            }
          <Box mt={5}>
            <Copyright />
          </Box>
        </form>
      </div>
    </AuthLayout>
  )
}

export default ForgotPasswordPage
