export const isEmailValid = (email) => {
  return new Promise(async (resolve, reject) => {

    const re =
      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    const solution = re.test(String(email).toLowerCase())

    resolve({ result: solution });
  });
};


