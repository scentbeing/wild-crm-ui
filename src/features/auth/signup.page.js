import React, { useState, useEffect } from 'react'
import { navigate } from "gatsby"
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Alert from '@material-ui/lab/Alert';

import AuthLayout from "../../layouts/AuthLayout"
import { canUserSignUp, request, signIn, signUp } from "./auth.store"
import { isEmailValid } from "./auth.validation"


function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
      <Link color="inherit" href="https://mui.com/">
        Your Website
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

const useStyles = makeStyles((theme) => ({
  root: {
    height: '100vh',
  },
  image: {
    backgroundImage: 'url(https://source.unsplash.com/random)',
    backgroundRepeat: 'no-repeat',
    backgroundColor:
      theme.palette.type === 'light' ? theme.palette.grey[50] : theme.palette.grey[900],
    backgroundSize: 'cover',
    backgroundPosition: 'center',
  },
  paper: {
    margin: theme.spacing(8, 4),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

function LoginPage({ location }) {
  const classes = useStyles();

  const [signupLink, setSignupLink] = useState("/login")
  // const [forgotPasswordLink, setForgotPasswordLink] = useState("/forgot-password")
  // const [signupLink, setSignupLink] = useState("/sign-up")
  const [serverPasswordInput, setServerPasswordInput] = useState(null)
  const [serverPasswordInputError, setserverPasswordInputError] = useState(null)
  const [nameInput, setNameInput] = useState("")
  const [nameInputError, setNameInputError] = useState(null)
  const [usernameInput, setUsernameInput] = useState("")
  const [usernameInputError, setUsernameInputError] = useState(null)
  const [emailInput, setEmailInput] = useState("")
  const [emailInputError, setEmailInputError] = useState(null)
  const [passwordInput, setPasswordInput] = useState("")
  const [passwordInputError, setPasswordInputError] = useState(null)
  const [confirmPasswordInput, setConfirmPasswordInput] = useState("")
  const [confirmPasswordInputError, setConfirmPasswordInputError] = useState(null)
  const [errorMessage, setErrorMessage] = useState(null)


  const [view, setView] = useState(null) // signup, request, not-allowed, thank-you-request
  // const [serverPassword, setServerPassword] = useState(null)

  useEffect(() => {
    const init = async () => {
      const canUserSignUpObj = await canUserSignUp();

      if (canUserSignUpObj.result) {
        setView("signup")
      } else if (!canUserSignUpObj.result && canUserSignUpObj.message === "Use user request.") {
        setView("request")
      } else if (!canUserSignUpObj.result && canUserSignUpObj.message === "Use user request with password.") {
        setView("request")
        setServerPasswordInput("")
      } else {
        setView("not-allowed")
      }

      if (location.search) {
        setSignupLink(`${signupLink}${location.search}`)
      }
    }

    init()
  }, [])

  const handleUsernameInput = (event) => {
    setUsernameInput(event.target.value)
  }

  const handleEmailInput = (event) => {
    setEmailInput(event.target.value)
  }

  const handlePasswordInput = (event) => {
    setPasswordInput(event.target.value)
  }

  const handleConfirmPasswordInput = (event) => {
    setConfirmPasswordInput(event.target.value)
  }

  const handleServerPasswordInput = (event) => {
    setServerPasswordInput(event.target.value)
  }

  const handleNameInput = (event) => {
    setNameInput(event.target.value)
  }

  const resetErrors = async () => {
    setEmailInputError(null)
    setNameInputError(null)
    setUsernameInputError(null)
    setPasswordInputError(null)
    setConfirmPasswordInputError(null)
    setserverPasswordInputError(null)
    setErrorMessage(null)
  }

  const handleRequestSubmit = async (event) => {
    event.preventDefault();

    let hasErrors = false;
    await resetErrors()

    if (serverPasswordInput !== null && serverPasswordInput.length === 0) {
      setserverPasswordInputError("Please enter a value.")
      hasErrors = true
    }

    const emailValid = await isEmailValid(emailInput)
    if (!emailValid.result || emailInput.length === 0) {
      setEmailInputError("Please enter your email address.")
      hasErrors = true
    }

    if (nameInput.length === 0) {
      setNameInputError("Please enter your name.")
      hasErrors = true
    }

    if (usernameInput.length === 0) {
      setUsernameInputError("Please enter your username.")
      hasErrors = true
    }

    if (passwordInput !== confirmPasswordInput) {
      setPasswordInputError("Passwords must match.")
      setConfirmPasswordInputError("Passwords must match.")
      hasErrors = true
    }

    if (passwordInput.length === 0) {
      setPasswordInputError("Please enter a password.")
      hasErrors = true
    }

    if (hasErrors) {
      return;
    }

    const userRequestResult = await request({
      email: emailInput,
      password: passwordInput,
      username: usernameInput,
      name: nameInput,
      serverPassword: serverPasswordInput
    })

    if (userRequestResult?.errors) {
      setErrorMessage(userRequestResult?.errors[0].message)

      if (userRequestResult?.errors[0].message === "Server password is incorrect.") {
        setserverPasswordInputError("Server password is incorrect.")
      }

      return
    }

    setView("thank-you-request")
    // const urlParams = new URLSearchParams(location.search)
    // if (urlParams.get("return_url")) {
    //   navigate(decodeURI(urlParams.get("return_url")))

    // } else {

    //   navigate("/dashboard")
    // }

  }

  const handleSignupSubmit = async (event) => {
    event.preventDefault();

    let hasErrors = false;
    await resetErrors()

    const emailValid = await isEmailValid(emailInput)
    if (!emailValid.result || emailInput.length === 0) {
      setEmailInputError("Please enter your email address.")
      hasErrors = true
    }

    if (usernameInput.length === 0) {
      setUsernameInputError("Please enter your username.")
      hasErrors = true
    }

    if (passwordInput !== confirmPasswordInput) {
      setPasswordInputError("Passwords must match.")
      setConfirmPasswordInputError("Passwords must match.")
      hasErrors = true
    }

    if (passwordInput.length === 0) {
      setPasswordInputError("Please enter a password.")
      hasErrors = true
    }

    if (hasErrors) {
      return;
    }

    const letsSignUp = await signUp({
      email: emailInput,
      password: passwordInput,
      username: usernameInput
    })

    if (letsSignUp?.errors) {
      setErrorMessage(letsSignUp?.errors[0].message)

      return
    }
    

    // setView("thank-you-request")

    const urlParams = new URLSearchParams(location.search)
    if (urlParams.get("return_url")) {
      navigate(decodeURI(urlParams.get("return_url")))

    } else {

      navigate("/dashboard")
    }
  }

  return (
    <AuthLayout>
      <div className={classes.paper}>
        <Grid container>
          <Grid item xs>
            <Link href={signupLink} variant="body2">
              Go Back to Sign In
            </Link>
          </Grid>
          {/* <Grid item>
              <Link href="#" variant="body2">
                {"Don't have an account? Sign Up"}
              </Link>
            </Grid> */}
        </Grid>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Sign Up
        </Typography>

        {view && view === "not-allowed" && (

          <Alert severity="warning">This application is currently invite only.</Alert>

        )}

        {view && view === "thank-you-request" && (
          <Alert severity="success">Your request to become a user will be reviewed. Please check your email.</Alert>

        )}

        {view && view === "signup" && (
          <form className={classes.form} noValidate onSubmit={handleSignupSubmit}>
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="username"
              label="Username"
              name="username"
              // autoComplete="username"
              autoFocus
              value={usernameInput}
              onChange={handleUsernameInput}
              error={usernameInputError && usernameInputError.length > 0}
              helperText={usernameInputError}
            />
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="email"
              label="Email Address"
              name="email"
              // autoComplete="email"
              value={emailInput}
              onChange={handleEmailInput}
              error={emailInputError && emailInputError.length > 0}
              helperText={emailInputError}
            />
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              name="password"
              label="Password"
              type="password"
              id="password"
              // autoComplete="current-password"
              value={passwordInput}
              onChange={handlePasswordInput}
              error={passwordInputError && passwordInputError.length > 0}
              helperText={passwordInputError}
            />
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              name="corfirm-password"
              label="Confirm Password"
              type="corfirm-password"
              id="confirm-password"
              // autoComplete="current-password"
              value={confirmPasswordInput}
              onChange={handleConfirmPasswordInput}
              error={confirmPasswordInputError && confirmPasswordInputError.length > 0}
              helperText={confirmPasswordInputError}
            />
            {/* <FormControlLabel
              control={<Checkbox value="remember" color="primary" />}
              label="Remember me"
            /> */}
            {errorMessage && (
              <Alert severity="error">
                {errorMessage}
              </Alert>
            )}
            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
            >
              Sign Up
            </Button>
          </form>
        )}







        {view && view === "request" && (
          <form className={classes.form} noValidate onSubmit={handleRequestSubmit}>
            <Alert severity="warning">
              This application is accepting request to become a user.
              {serverPasswordInput !== null && (<TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                id="server-password"
                label="Server Password"
                name="server-password"
                // autoComplete="email"
                value={serverPasswordInput}
                onChange={handleServerPasswordInput}
                error={serverPasswordInputError && serverPasswordInputError.length > 0}
                helperText={serverPasswordInputError}
              />
              )}

            </Alert>

            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="username"
              label="Username"
              name="username"
              // autoComplete="username"
              autoFocus={true}

              value={usernameInput}
              onChange={handleUsernameInput}
              error={usernameInputError && usernameInputError.length > 0}
              helperText={usernameInputError}
            />
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="name"
              label="Name"
              name="name"
              // autoComplete="username"
              autoFocus
              value={nameInput}
              onChange={handleNameInput}
              error={nameInputError && nameInputError.length > 0}
              helperText={nameInputError}
            />
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="email"
              label="Email Address"
              name="email"
              // autoComplete="email"
              value={emailInput}
              onChange={handleEmailInput}
              error={emailInputError && emailInputError.length > 0}
              helperText={emailInputError}
            />
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              name="password"
              label="Password"
              type="password"
              id="password"
              // autoComplete="current-password"
              value={passwordInput}
              onChange={handlePasswordInput}
              error={passwordInputError && passwordInputError.length > 0}
              helperText={passwordInputError}
            />
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              name="confirm-password"
              label="Confirm Password"
              type="password"
              id="confirm-password"
              // autoComplete="current-password"
              value={confirmPasswordInput}
              onChange={handleConfirmPasswordInput}
              error={confirmPasswordInputError && confirmPasswordInputError.length > 0}
              helperText={confirmPasswordInputError}
            />
            {/* <FormControlLabel
              control={<Checkbox value="remember" color="primary" />}
              label="Remember me"
            /> */}
            {errorMessage && (
              <Alert severity="error">
                {errorMessage}
              </Alert>
            )}
            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
            >
              Sign Up
            </Button>
          </form>
        )}

        <Box mt={5}>
          <Copyright />
        </Box>
      </div>
    </AuthLayout>
  )
}

export default LoginPage
