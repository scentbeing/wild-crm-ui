import axios from "axios"

let currentUser;
let currentToken;

export const user = {}

export const signIn = async ({ email, password }) => {
  var query = `
  mutation signinForm($email: String!, $password: String!) {
    signin(email: $email, password: $password) {
      token
      userId
    }
  }`

  const url = getUrl();

  const response = await axios.post(url, {
    query,
    variables: { email, password },
  })

  if (response?.data?.errors) {
    return { result: false, message: response?.data?.errors[0].message }
  }

  currentToken = response.data.data.signin.token
  let userId = response.data.data.signin.userId

  let userQuery = `
  query getCurrentUser($id : ID!){
    user(id: $id) {
      id
      username
      email
      profile {
        name
        birthday
        location
        website
        picture
      }
      permissionMany(pageSize: 100) {
        count
        page
        pageSize
        pageCount
        rows {
          id
          name
        }
      }
      roleMany(pageSize: 100) {
        count
        page
        pageSize
        pageCount
        rows {
          id
          name
          permissionMany(pageSize: 100) {
            count
            page
            pageSize
            pageCount
            rows {
              id
              name
            }
          }
        }
      }
    }
  }
  `
  currentUser = (await getData({
    query: userQuery,
    variables: {
      id: userId
    }
  })).data.user

  console.log('currentUser', currentUser)

  localStorage.setItem("current.entity", JSON.stringify({
    currentUser,
    currentToken
  }))

  return {
    result: true,
    data: {
      currentUser,
      currentToken
    }
  }
}

export const getData = ({ query, variables }) => {
  return new Promise(async (resolve, reject) => {

    const url = getUrl();

    const response = await axios.post(url, {
      query,
      variables,
    }, {
      headers: {
        ...getHeader({ token: currentToken })
      }
    }
    )

    console.log("response", response.data)

    resolve(response.data)
  })
}

export const getHeader = ({ token }) => {
  return { "Authorization": `Bearer ${token}` }
}

export const getUrl = () => {
  console.log("process.env", process.env)
  const serverUrl = process.env.SERVER_URL;
  const serverUrlCharacters = serverUrl.split('').filter(s => s.length !== 0);


  console.log('serverUrlCharacters', process.env.SERVER_URL, serverUrlCharacters)

  const doesServerUrlEndInSlash = serverUrlCharacters[serverUrlCharacters.length - 1] === "/"

  if (doesServerUrlEndInSlash) {
    serverUrlCharacters.splice(serverUrlCharacters.length - 1, 1)
  }

  return `${serverUrl}/graphql`
}

export const isSignedIn = async () => {
  const currentEntity = localStorage.getItem("current.entity")


  console.log("currentEntity", currentEntity)
  if (!currentEntity) {
    return { result: false }
  }

  const currentEntityParsed = JSON.parse(currentEntity);

  console.log('currentEntityParsed', currentEntityParsed)

  const getUserIdQuery = `
  query getUserId($token : String!) {
    getuserIdFromToken(token: $token) {
      userId
    }
  }`

  const getUserIdResult = await getData({
    query: getUserIdQuery,
    variables: { token: currentEntityParsed.currentToken }
  })

  console.log('getUserIdResult', getUserIdResult)

  if (!getUserIdResult.data.getuserIdFromToken.userId) {
    return { result: false }
  }

  if (getUserIdResult.data.getuserIdFromToken.userId.toString() !== currentEntityParsed.currentUser.id.toString()) {
    await signOut()
    return { result: false }
  }

  currentUser = currentEntityParsed.currentUser
  currentToken = currentEntityParsed.currentToken

  return { result: true }
}

export const signOut = async () => {
  localStorage.removeItem("current.entity")
  currentUser = null;
  currentToken = null;
}

export const sendForgotPasswordRequest = async ({ email }) => {

  const query = ` 
  mutation forgotPasswordEmail($email: String!) {
    forgotPassword(email: $email) {
      result
      message
      data
    }
  }
  `

  return (await getData({ query, variables: { email } })).data

}

export const canUserSignUp = async () => {

  const query = ` 
  {
    canUserSignUp {
      result
      message
    }
  }
  `
  const result = await getData({ query });
  return result.data.canUserSignUp
}

export const signUp = async ({ email, password, username }) => {

  const query = ` 
  mutation addUserRequest ($email: String!, $password: String!, $username: String!){
    signup(email: $email, password: $password, username: $username, ) {
      token
      userId
    }
  }`

  const result = await getData({
    query,
    variables: {
      email,
      password,
      username
    }
  });

  console.log('result!!!', result)

  if (result?.errors) {
    return result
  }

  currentToken = result.data.signup.token

  let userId = result.data.signup.userId

  let userQuery = `
  query getCurrentUser($id : ID!){
    user(id: $id) {
      id
      username
      email
      profile {
        name
        birthday
        location
        website
        picture
      }
      permissionMany(pageSize: 100) {
        count
        page
        pageSize
        pageCount
        rows {
          id
          name
        }
      }
      roleMany(pageSize: 100) {
        count
        page
        pageSize
        pageCount
        rows {
          id
          name
          permissionMany(pageSize: 100) {
            count
            page
            pageSize
            pageCount
            rows {
              id
              name
            }
          }
        }
      }
    }
  }
  `
  currentUser = (await getData({
    query: userQuery,
    variables: {
      id: userId
    }
  })).data.user

  console.log('currentUser', currentUser)

  localStorage.setItem("current.entity", JSON.stringify({
    currentUser,
    currentToken
  }))


  return result.data.signup
}

export const request = async ({ email, password, username, name, serverPassword }) => {
  const query = ` 

  mutation addUserRequest ($email: String!, $password: String!, $username: String!, $name: String! ${serverPassword ? ", $serverPassword: String!" : ""}){
    userRequestAdd(email: $email, password: $password, username: $username, name: $name ${serverPassword ? ", serverPassword: $serverPassword" : ""}) {
      id
    }
  }
    `
  const result = await getData({
    query,
    variables: {
      email,
      password,
      username,
      name,
      serverPassword,
    }
  });


  console.log('fix result', result)
  return result
}

