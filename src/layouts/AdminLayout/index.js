import React from 'react'
import Context from "./context"
import View from "./view"



function index( props ) {
	return (
		<Context>
				<View {...props} />
		</Context>
	)
}

export default index
