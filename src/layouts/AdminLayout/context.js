import React from 'react'
import { SpeedDialProvider } from './SpeedDial.context'
// import { MeetingDrawerProvider } from './MeetingDrawer.context'
import { MeetingPortalProvider } from "./Meeting/MeetingPortal.context"
import { NewMeetingModalProvider } from "./Meeting/modals/NewMeetingModal/NewMeetingModal.context"
import { EditMeetingModalProvider } from "./Meeting/modals/EditMeetingModal/EditMeetingModal.context"
import { KickUserMeetingModalProvider } from "./Meeting/modals/KickUserMeetingModal/KickUserMeetingModal.context"
import { ChangeLeaderMeetingModalProvider } from "./Meeting/modals/ChangeLeaderMeetingModal/ChangeLeaderMeetingModal.context"


const ModalProviders = ({ children }) => {
  return (

    <NewMeetingModalProvider>
      <EditMeetingModalProvider>
        <KickUserMeetingModalProvider>
          <ChangeLeaderMeetingModalProvider>
            {children}
          </ChangeLeaderMeetingModalProvider>
        </KickUserMeetingModalProvider>
      </EditMeetingModalProvider>
    </NewMeetingModalProvider>
  )
}


function context({ children }) {
  return (
    <SpeedDialProvider>
      <MeetingPortalProvider>
        <ModalProviders>
          {children}
        </ModalProviders>
      </MeetingPortalProvider>
    </SpeedDialProvider>
  )
}

export default context
