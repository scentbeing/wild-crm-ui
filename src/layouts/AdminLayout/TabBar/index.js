import React from 'react'
import PropTypes from 'prop-types'
import AppBar from '@material-ui/core/AppBar'
import Tab from '@material-ui/core/Tab'
import Tabs from '@material-ui/core/Tabs'
import { withStyles } from '@material-ui/core/styles'
import { navigate } from 'gatsby-link'

const styles = theme => ({
	secondaryBar: {
		zIndex: 0,
	},
})

function TabBar({ classes, tabs = [] }) {
	
	let activeIndex = 0;
	
	tabs.map((tab, i) => {
		if (tab.isActive || tab.active) {
			activeIndex = i
		}
	})

	return (
		<AppBar component="div" className={classes.secondaryBar} color="primary" position="static" elevation={0}>
			<Tabs value={activeIndex} textColor="inherit">
				{tabs.map((tab, i) => (
					<Tab key={i} textColor="inherit" label={`${tab.name}`} onClick={() => navigate(tab.url)} />
				))}
			</Tabs>
		</AppBar>
	)
}

TabBar.propTypes = {
	classes: PropTypes.object.isRequired,
	tabNames: PropTypes.array.isRequired,
}

export default withStyles(styles)(TabBar)
