import React, { useState } from 'react'

const MeetingDrawerContext = React.createContext(null);

export const MeetingDrawerProvider = ({ children }) => {

  const [isOpened, setIsOpened] = useState(false);
  // const [editing, toggleEditor] = useState(false);

  const value = React.useMemo(() => {
    return {
      isOpened,
      setIsOpened,
    }
  }, [isOpened]);

  return (
    <MeetingDrawerContext.Provider value={value}>
      {children}
    </MeetingDrawerContext.Provider>
  )
}

export default MeetingDrawerContext
