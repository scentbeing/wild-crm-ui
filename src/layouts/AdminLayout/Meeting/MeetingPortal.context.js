import React, { useState, useMemo } from 'react'

const MeetingPortalContext = React.createContext(null)

export const MeetingPortalProvider = ({ children }) => {

  const [isOpened, setIsOpened] = useState(false);
  const [isWindowOpened, setIsWindowOpened] = useState(false);
  const [meetingWindowView, setMeetingWindowView] = useState("meetings"); // meetings, meeting
  const [meetingList, setMeetingList] = useState([])
  const [usersInMeeting, setUsersInMeeting] = useState([{ id: 1, email: "bob@bob.com", username: "Bob Dole" }, { id: 2, email: "bugs@acme.com" }])
  const [usersAvailableToJoin, setUsersAvailableToJoin] = useState([{ id: 1, email: "bob@bob.com", username: "Bob Dole" }, { id: 2, email: "bugs@acme.com" }])
  const [usersOffLine, setUsersOffLine] = useState([{ id: 1, email: "bob@bob.com", username: "Bob Dole" }, { id: 2, email: "bugs@acme.com" }])
  const [meetingObject, setMeetingObject] = useState({})

  const value = {
    isOpened,
    setIsOpened,
    isWindowOpened,
    setIsWindowOpened,
    meetingList,
    setMeetingList,
    meetingWindowView,
    setMeetingWindowView,
    usersInMeeting,
    setUsersInMeeting,
    usersAvailableToJoin,
    setUsersAvailableToJoin,
    usersOffLine,
    setUsersOffLine,
    meetingObject,
    setMeetingObject,
  }

  return (
    <MeetingPortalContext.Provider value={value}>
      {children}
    </MeetingPortalContext.Provider>
  )
}

export default MeetingPortalContext
