import React, { useContext } from 'react'
import { useSpring, animated } from '@react-spring/web'
//Material
import { makeStyles } from '@material-ui/core/styles'
import Typography from '@material-ui/core/Typography'
import IconButton from '@material-ui/core/IconButton'
//icons
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import CancelIcon from '@material-ui/icons/Cancel';
//context
import MeetingPortalContext from "./MeetingPortal.context"
//components
import Meetings from './windows/Meetings'
import Meeting from './windows/Meeting'

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    justifyContent: 'center',
    flexWrap: 'wrap',
    '& > *': {
      margin: theme.spacing(0.5),
    },
  },
  panel: {
    position: "absolute",
    bottom: 0,
    left: "10px",
    backgroundColor: theme.palette.grey["300"],
    border: `1px solid ${theme.palette.grey["500"]}`,
    padding: "5px 5px 5px 10px",
    // zIndex: 1000,
    height: "42px",
    width: "250px",
    cursor: "pointer"
  },
  endMeetingButton: {
    float: "right",
    color: theme.palette.error.main,

  },
  // panelClose: {
  //   position: "relative",
  //   backgroundColor: "red",
  //   color: "white",
  //   display: "inline-block",
  //   padding: "5px 8px",
  //   borderRadius: "50%",
  //   marginLeft: "36px",
  //   height: "25px",
  //   width: "25px",
  //   fontWeight: "bolder",
  //   cursor: "pointer",
  // },
  window: {
    position: "absolute",
    bottom: "41px",
    left: "10px",
    width: "300px",
    height: "400px",
    border: `1px solid ${theme.palette.grey["500"]}`,
    backgroundColor: theme.palette.grey["300"],
    overflow: "hidden",
  },
  windowHeader: {
    display: "block",
    height: "50px",
    width: "100%",
    backgroundColor: theme.palette.primary.main,
    color: theme.palette.primary.contrastText,
    padding: " 10px",
  },
  windowBody: {
    display: "block",
    height: "calc(100% - 50px)",
    width: "100%",
    backgroundColor: theme.palette.grey["200"],
    border: `1px solid ${theme.palette.grey["500"]}`,
    color: theme.palette.grey["800"],
    overflowY: "auto"
  },
  windowLowerButton: {
    float: "right",
    color: theme.palette.primary.contrastText,

  }
}));


function MeetingPortal() {
  const meetingPortalContext = useContext(MeetingPortalContext)
  
  // const styles = useSpring({ to: { opacity: 1 }, from: { opacity: 0 } })
  console.log("meetingPortalContext", meetingPortalContext)
  const classes = useStyles();

  const styles = useSpring({

    height: meetingPortalContext.isWindowOpened ? "400px" : "0px",
    borderWidth: meetingPortalContext.isWindowOpened ? `1px` : "0px"


  })

  const handleClose = (event) => {
    event.preventDefault();
    meetingPortalContext.setIsOpened(false)
  }

  const handleOpenWindow = (event) => {
    event.preventDefault();
    meetingPortalContext.setIsWindowOpened(true)
  }

  return (
    <>
      <div className={`${classes.panel}`} onClick={handleOpenWindow}>
        <IconButton
          aria-label="delete"
          className={`${classes.endMeetingButton}`}
          onClick={handleClose}
        >
          <CancelIcon />
        </IconButton>
        {/* Meetings */}

        <Typography variant="h6" gutterBottom>
          Meetings
        </Typography>
        {/* <div className={`${classes.panelClose}`} onClick={handleClose}>X</div> */}

      </div>
      <animated.div className={`${classes.window}`} style={styles}>
        <div className={`${classes.windowHeader}`}>
          <IconButton aria-label="delete" className={`${classes.windowLowerButton}`}>
            <KeyboardArrowDownIcon onClick={() => meetingPortalContext.setIsWindowOpened(false)} />
          </IconButton>

          <Typography variant="h5" gutterBottom>
            Meetings
          </Typography>

        </div>
        <div className={`${classes.windowBody}`}>
          {meetingPortalContext.meetingWindowView === "meetings" && (<Meetings />)}
          {meetingPortalContext.meetingWindowView === "meeting" && (<Meeting />)}

        </div>
      </animated.div>
    </>
  )
}

export default MeetingPortal