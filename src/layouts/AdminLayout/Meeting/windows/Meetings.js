import React, { useContext } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListSubheader from '@material-ui/core/ListSubheader';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import IconButton from '@material-ui/core/IconButton';
import NewMeetingModalContext from "../modals/NewMeetingModal/NewMeetingModal.context"
import MeetingPortalContext from "../MeetingPortal.context"
import MeetingsStore from "./Meetings.store"


const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    maxWidth: 360,
    // backgroundColor: theme.palette.background.paper,
    position: 'relative',
    overflow: 'auto',
    // maxHeight: 300,
  },
  listSection: {
    backgroundColor: 'inherit',
  },
  ul: {
    backgroundColor: 'inherit',
    padding: 0,
  },
  linkButton: {
    textDecoration: "underline",
    cursor: "pointer"
  },
  green: {
    color: theme.palette.success.main
  }
}));

export default function Meetings() {
  const newMeetingModalContext = useContext(NewMeetingModalContext)
  const meetingPortalContext = useContext(MeetingPortalContext)

  const classes = useStyles();

  const handleCreateMeeting = () => {
    newMeetingModalContext.setIsOpened(true)
  }

  const handleJoin = (id) => {
    MeetingsStore.joinMeeting({id})
  }

  return (
    <List className={classes.root} subheader={<li />}>
      {/* {[0, 1, 2, 3, 4,8,9,2].map((sectionId) => ( */}
      <li className={classes.listSection}>
        <ul className={classes.ul}>
          <ListItem>
            <ListItemText
              // primary="Oui Oui"
              secondary={"Name"} />
            {/* <ListSubheader>Name</ListSubheader> */}
            <ListItemSecondaryAction className={`${classes.linkButton} ${classes.green}`} onClick={handleCreateMeeting}>
              {/* <IconButton edge="end" aria-label="comments"> */}
              Create
              {/* </IconButton> */}
            </ListItemSecondaryAction>
          </ListItem>
          {meetingPortalContext.meetingList.map((item, i) => (
            <ListItem key={`item-${item.id}`}>
              <ListItemText primary={`${item.name}`} />
              <ListItemSecondaryAction className={`${classes.linkButton}`} onClick={() => handleJoin(item.id)}>
                {/* <IconButton edge="end" aria-label="comments"> */}
                Join
                {/* </IconButton> */}
              </ListItemSecondaryAction>
            </ListItem>
          ))}
          {meetingPortalContext.meetingList.length === 0 && (
          <ListItem>
            <ListItemText
              primary="No meetings at the moment."
              // secondary={"Name"} 
              />
            {/* <ListSubheader>Name</ListSubheader> */}
          </ListItem>

          ) }
        </ul>
      </li>
      {/* ))} */}
    </List>
  );
}