import React, { useContext, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import MeetingsStore from "./Meetings.store"
import EditIcon from '@material-ui/icons/Edit';
import AddIcon from '@material-ui/icons/Add';
import DoneIcon from '@material-ui/icons/Done';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';

//context
import MeetingPortalContext from "../MeetingPortal.context"
// import NewMeetingModalContext from "../modals/NewMeetingModal/NewMeetingModal.context"
import EditMeetingModalContext from '../modals/EditMeetingModal/EditMeetingModal.context';
import ChangeLeaderMeetingModalContext from '../modals/ChangeLeaderMeetingModal/ChangeLeaderMeetingModal.context';
import KickUserMeetingModalContext from '../modals/KickUserMeetingModal/KickUserMeetingModal.context';
import { inviteUser } from './Meeting.store';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    maxWidth: 360,
    // backgroundColor: theme.palette.background.paper,
    position: 'relative',
    overflow: 'auto',
    // maxHeight: 300,
  },
  listSection: {
    backgroundColor: 'inherit',
  },
  ul: {
    backgroundColor: 'inherit',
    padding: 0,
  },
  linkButton: {
    textDecoration: "underline",
    cursor: "pointer",
  },
  warning: {
    color: theme.palette.warning.main
  },
  success: {
    color: theme.palette.success.main
  },
  danger: {
    color: theme.palette.error.main
  }
}));

export default function Meeting() {
  const classes = useStyles();

  //init context
  // const newMeetingModalContext = useContext(NewMeetingModalContext)
  const meetingPortalContext = useContext(MeetingPortalContext)
  const editMeetingModalContext = useContext(EditMeetingModalContext)
  const changeLeaderMeetingModalContext = useContext(ChangeLeaderMeetingModalContext)
  const kickUserMeetingModalContext = useContext(KickUserMeetingModalContext)



  const handleEditMeetingName = () => {
    editMeetingModalContext.setIsOpened(true)
    // editMeetingModalContext.setIsOpened(true)
  }

  window.kickUserMeetingModalContext = kickUserMeetingModalContext;
  const handleUserKick = ({ user }) => {
    kickUserMeetingModalContext.setSelectedUser({ id: 1, username: "Bugs Bunny" })
    kickUserMeetingModalContext.setIsOpened(true)
  }

  const handleLeaderChange = () => {
    changeLeaderMeetingModalContext.setIsOpened(true)
  }

  const AddUser = ({user}) => {
    const [selected, setSelected] = useState(false)

    const handleInvite = ({user}) => {
      if (!selected) {
        inviteUser({user})
      }

      setSelected(true)
    }

    return (
    <ListItemSecondaryAction className={` ${classes.success}`}>
      {/* <IconButton edge="end" aria-label="comments"> */}
      {/* <Button className={`${classes.success}`}> */}
      {!selected && (<AddIcon className={`${classes.linkButton}`} onClick={handleInvite} />)}
      {selected && (<DoneIcon />)}

      {/* </Button> */}
      {/* </IconButton> */}
    </ListItemSecondaryAction>
    )
  }

  return (
    <List className={classes.root} subheader={<li />}>
      {/* {[0, 1, 2, 3, 4,8,9,2].map((sectionId) => ( */}
      <li className={classes.listSection}>
        <ul className={`${classes.ul}`}>
          <ListItem>
            <ListItemText
              primary="Name"
              secondary={meetingPortalContext?.meetingObject?.name || "Undefined"} />
            {/* <ListSubheader>Name</ListSubheader> */}
            <ListItemSecondaryAction className={`${classes.warning}`} onClick={handleEditMeetingName}>
              {/* <IconButton edge="end" aria-label="comments"> */}
              {/* <Button className={`${classes.warning}`}> */}
              <EditIcon className={`${classes.linkButton}`} />
              {/* </Button> */}
              {/* </IconButton> */}
            </ListItemSecondaryAction>
          </ListItem>


          <ListItem>
            <ListItemText
              primary="Leader"
              secondary={`${meetingPortalContext?.leader?.profile?.name || meetingPortalContext?.leader?.username || meetingPortalContext?.leader?.email || "Undefined"}`} />
            {/* <ListSubheader>Name</ListSubheader> */}
            <ListItemSecondaryAction className={`${classes.linkButton} ${classes.warning}`} onClick={handleLeaderChange}>
              {/* <IconButton edge="end" aria-label="comments"> */}
              {/* <Button className={`${classes.warning}`}> */}
              <EditIcon className={`${classes.linkButton}`} />
              {/* </Button> */}
              {/* </IconButton> */}
            </ListItemSecondaryAction>
          </ListItem>


          <ListItem>
            <ListItemText
              primary="Users In Meeting" />
          </ListItem>

          {meetingPortalContext.usersInMeeting.map((user, i) => (
            <ListItem key={`item-${user.id}`}>
              <ListItemText secondary={`${user?.profile?.name || user.username || user.email}`} />
              <ListItemSecondaryAction className={`${classes.danger}`} onClick={handleUserKick}>
                {/* <IconButton edge="end" aria-label="comments"> */}
                <ExitToAppIcon  className={`${classes.linkButton}`} />
                {/* <DropDown dropDownOptions={[
                  {
                    title: "Transfer Leadership",
                  }, {
                    title: "Kick",
                  },
                ]} /> */}
                {/* <AddIcon /> */}
                {/* </IconButton> */}
              </ListItemSecondaryAction>
            </ListItem>
          ))}
          {meetingPortalContext.usersInMeeting.length === 0 && (
            <ListItem>
              <ListItemText
                secondary="No users."
              // secondary={"Name"} 
              />
              {/* <ListSubheader>Name</ListSubheader> */}
            </ListItem>
          )}

          <br />
          <ListItem>
            <ListItemText
              primary="Online Users" />
          </ListItem>



          {meetingPortalContext.usersAvailableToJoin.map((user, i) => (
            <ListItem key={`item-${user.id}`}>
              <ListItemText secondary={`${user?.profile?.name || user.username || user.email}`} />
              <AddUser user={user} />
            </ListItem>
          ))}
          {meetingPortalContext.usersAvailableToJoin.length === 0 && (
            <ListItem>
              <ListItemText
                secondary="No users."
              // secondary={"Name"} 
              />
              {/* <ListSubheader>Name</ListSubheader> */}
            </ListItem>
          )}

          <br />
          <ListItem>
            <ListItemText
              primary="Offline Users" />
          </ListItem>



          {meetingPortalContext.usersOffLine.map((user, i) => (
            <ListItem key={`item-${user.id}`}>
              <ListItemText secondary={`${user?.profile?.name || user.username || user.email}`} />
            </ListItem>
          ))}
          {meetingPortalContext.usersOffLine.length === 0 && (
            <ListItem>
              <ListItemText
                secondary="No users."
              // secondary={"Name"} 
              />
              {/* <ListSubheader>Name</ListSubheader> */}
            </ListItem>
          )}


        </ul>
      </li>
      {/* ))} */}
    </List>
  );
}