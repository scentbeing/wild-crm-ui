import React, { useState } from 'react'

const EditMeetingModalContext = React.createContext(null);

export const EditMeetingModalProvider = ({ children }) => {

  const [isOpened, setIsOpened] = useState(false);

  const value = {
    isOpened,
    setIsOpened,
  }

  return (
    <EditMeetingModalContext.Provider value={value}>
      {children}
    </EditMeetingModalContext.Provider>
  )
}

export default EditMeetingModalContext
