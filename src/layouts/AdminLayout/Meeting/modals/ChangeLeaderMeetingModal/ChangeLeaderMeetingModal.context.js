import React, { useState, useMemo } from 'react'

const ChangeLeaderMeetingModalContext = React.createContext(null);

export const ChangeLeaderMeetingModalProvider = ({ children }) => {

  const [isOpened, setIsOpened] = useState(false);

  const value = {
    isOpened,
    setIsOpened,
  }

  return (
    <ChangeLeaderMeetingModalContext.Provider value={value}>
      {children}
    </ChangeLeaderMeetingModalContext.Provider>
  )
}

export default ChangeLeaderMeetingModalContext
