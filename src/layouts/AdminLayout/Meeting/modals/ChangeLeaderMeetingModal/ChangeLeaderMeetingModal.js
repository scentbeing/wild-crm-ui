import React, { useContext, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';

import ChangeLeaderMeetingModalContext from "./ChangeLeaderMeetingModal.context"
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { createMeeting } from './ChangeLeaderMeetingModal.store';
import SelectUser from '../../../../../components/SelectUser/SelectUser';

function rand() {
  return Math.round(Math.random() * 20) - 10;
}

function getModalStyle() {
  const top = 50 + rand();
  const left = 50 + rand();

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}

const useStyles = makeStyles((theme) => ({
  paper: {
    position: 'absolute',
    width: 400,
    backgroundColor: theme.palette.background.paper,
    border: '2px solid #000',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
  input: {
    display: "block",
    width: "100%",
    marginBottom: "15px",
    "& > div": {
      width: "100%"
    }
  },
  cancel: {
    float: "right",
  }
}));

export default function ChangeLeaderMeetingModal() {
  const classes = useStyles();

  const changeLeaderMeetingModalContext = useContext(ChangeLeaderMeetingModalContext)
  // getModalStyle is not a pure function, we roll the style only on the first render

  const [modalStyle] = useState(getModalStyle);
  const [nameInput, setNameInput] = useState("")
  const [nameInputError, setNameInputError] = useState(null)

  const handleOpen = () => {
    changeLeaderMeetingModalContext.setIsOpened(true);
  };

  const handleClose = () => {
    resetFormErrors()
    setNameInput("")
    changeLeaderMeetingModalContext.setIsOpened(false);
  };

  const resetFormErrors = () => {
    setNameInputError(null)
  }

  const handleSubmit = event => {
    event.preventDefault()
    resetFormErrors();


    if (nameInput === "") {
      setNameInputError("Please enter a value.")
      return;
    }

    createMeeting({ name: nameInput })

    changeLeaderMeetingModalContext.setIsOpened(false);

  }

  const body = (
    <div style={modalStyle} className={classes.paper}>
      <h2 id="simple-modal-title">Change Leader</h2>

      <form className={classes.root} onSubmit={handleSubmit} autoComplete="off">

        <SelectUser />
        {/* <TextField
          // required
          classes={{ root: `${classes.input}` }}
          id="ChangeLeaderMeetingModal-name"
          label="Name"
          variant="filled"
          value={nameInput}
          onChange={event => setNameInput(event.target.value)}
          error={nameInputError && nameInputError.length > 0}
          helperText={nameInputError}

        /> */}

        <br/>
        <br/>

        <Button variant="contained" onClick={handleClose} className={`${classes.cancel}`}>
          Cancel
        </Button>


        <Button
          variant="contained"
          color="primary"
          type="submit"
        >
          CHANGE
        </Button>
      </form>
      {/* <SimpleModal /> */}
    </div>
  );

  return (
    // <div>
    //   <button type="button" onClick={handleOpen}>
    //     Open Modal
    //   </button>
    <Modal
      open={changeLeaderMeetingModalContext.isOpened}
      onClose={handleClose}
      aria-labelledby="simple-modal-title"
      aria-describedby="simple-modal-description"
    >
      {body}
    </Modal>
  );
}