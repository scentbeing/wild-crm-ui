import React, { useContext, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';

import KickUserMeetingModalContext from "./KickUserMeetingModal.context"
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { createMeeting, kickUser } from './KickUserMeetingModal.store';

import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';

function rand() {
  return Math.round(Math.random() * 20) - 10;
}

function getModalStyle() {
  const top = 50 + rand();
  const left = 50 + rand();

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}

const useStyles = makeStyles((theme) => ({
  paper: {
    position: 'absolute',
    width: 400,
    backgroundColor: theme.palette.background.paper,
    border: '2px solid #000',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
  input: {
    display: "block",
    width: "100%",
    marginBottom: "15px",
    "& > div": {
      width: "100%"
    }
  },
  cancel: {
    float: "right",
  },
  deleteButton: {
    // to make a red delete button
    color: theme.palette.getContrastText(theme.palette.error.main),
    background: theme.palette.error.main,
  }
}));

export default function KickUserMeetingModal() {
  const classes = useStyles();

  const kickUserMeetingModalContext = useContext(KickUserMeetingModalContext)

  // getModalStyle is not a pure function, we roll the style only on the first render
  const [modalStyle] = useState(getModalStyle);

  const user = kickUserMeetingModalContext.selectedUser

  const handleClose = () => {
    kickUserMeetingModalContext.setIsOpened(false);
  };

  const handleSubmit = event => {
    event.preventDefault()

    kickUser({ id: user.id })
    kickUserMeetingModalContext.setIsOpened(false);

  }

  const body = (
    <div style={modalStyle} className={classes.paper}>
      <h2 id="simple-modal-title">Kick User</h2>

      <form className={classes.root} onSubmit={handleSubmit} autoComplete="off">
        <List
          className={classes.root}
          subheader={<li />}
        >
          <ListItem>
            <ListItemText
              primary="Name"
              secondary={user?.profile?.name || user?.username || user?.email}
            />

          </ListItem>

        </List>

        <Button variant="contained" onClick={handleClose} className={`${classes.cancel}`}>
          Cancel
        </Button>


        <Button
          // variant="contained"
          // color="error"
          type="submit"
          // classes={{ root: `${classes.backgroundColor}` }}
        className={`${classes.deleteButton}`}
        >
          KICK USER
        </Button>
      </form>
      {/* <SimpleModal /> */}
    </div>
  );

  return (
    // <div>
    //   <button type="button" onClick={handleOpen}>
    //     Open Modal
    //   </button>
    <Modal
      open={kickUserMeetingModalContext.isOpened}
      onClose={handleClose}
      aria-labelledby="simple-modal-title"
      aria-describedby="simple-modal-description"
    >
      {body}
    </Modal>
  );
}