import React, { useState, useMemo } from 'react'

const KickUserMeetingModalContext = React.createContext(null);

export const KickUserMeetingModalProvider = ({ children }) => {

  const [isOpened, setIsOpened] = useState(false);

  //becomes user object: {id, name}
  const [selectedUser, setSelectedUser] = useState(null)

  const value = {
    isOpened,
    setIsOpened,
    selectedUser,
    setSelectedUser,
  }

  return (
    <KickUserMeetingModalContext.Provider value={value}>
      {children}
    </KickUserMeetingModalContext.Provider>
  )
}

export default KickUserMeetingModalContext
