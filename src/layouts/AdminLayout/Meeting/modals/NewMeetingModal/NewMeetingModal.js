import React, { useContext, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';

import NewMeetingModalContext from "./NewMeetingModal.context"
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { createMeeting } from './NewMeetingModal.store';

function rand() {
  return Math.round(Math.random() * 20) - 10;
}

function getModalStyle() {
  const top = 50 + rand();
  const left = 50 + rand();

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}

const useStyles = makeStyles((theme) => ({
  paper: {
    position: 'absolute',
    width: 400,
    backgroundColor: theme.palette.background.paper,
    border: '2px solid #000',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
  input: {
    display: "block",
    width: "100%",
    marginBottom: "15px",
    "& > div": {
      width: "100%"
    }
  },
  cancel: {
    float: "right",
  }
}));

export default function NewMeetingModal() {
  const classes = useStyles();

  const newMeetingModalContext = useContext(NewMeetingModalContext)
  // getModalStyle is not a pure function, we roll the style only on the first render

  const [modalStyle] = useState(getModalStyle);
  const [nameInput, setNameInput] = useState("")
  const [nameInputError, setNameInputError] = useState(null)

  const handleOpen = () => {
    newMeetingModalContext.setIsOpened(true);
  };

  const handleClose = () => {
    resetFormErrors()
    setNameInput("")
    newMeetingModalContext.setIsOpened(false);
  };

  const resetFormErrors = () => {
    setNameInputError(null)
  }

  const handleSubmit = event => {
    event.preventDefault()
    resetFormErrors();


    if (nameInput === "") {
      setNameInputError("Please enter a value.")
      return;
    }

    createMeeting({ name: nameInput })

    newMeetingModalContext.setIsOpened(false);

  }

  const body = (
    <div style={modalStyle} className={classes.paper}>
      <h2 id="simple-modal-title">Create A Meeting</h2>

      <form className={classes.root} onSubmit={handleSubmit} autoComplete="off">

        <TextField
          // required
          classes={{ root: `${classes.input}` }}
          id="NewMeetingModal-name"
          label="Name"
          variant="filled"
          value={nameInput}
          onChange={event => setNameInput(event.target.value)}
          error={nameInputError && nameInputError.length > 0}
          helperText={nameInputError}

        />

        <Button variant="contained" onClick={handleClose} className={`${classes.cancel}`}>
          Cancel
        </Button>


        <Button
          variant="contained"
          color="primary"
          type="submit"
        >
          Create Meeting
        </Button>
      </form>
      {/* <SimpleModal /> */}
    </div>
  );

  return (
    // <div>
    //   <button type="button" onClick={handleOpen}>
    //     Open Modal
    //   </button>
    <Modal
      open={newMeetingModalContext.isOpened}
      onClose={handleClose}
      aria-labelledby="simple-modal-title"
      aria-describedby="simple-modal-description"
    >
      {body}
    </Modal>
  );
}