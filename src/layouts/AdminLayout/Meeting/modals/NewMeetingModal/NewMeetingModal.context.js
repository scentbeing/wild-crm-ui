import React, { useState, useMemo } from 'react'

const NewMeetingModalContext = React.createContext(null);

export const NewMeetingModalProvider = ({ children }) => {

  const [isOpened, setIsOpened] = useState(false);

  const value = useMemo(() => {
    return {
      isOpened,
      setIsOpened,
    }
  }, [isOpened]);

  return (
    <NewMeetingModalContext.Provider value={value}>
      {children}
    </NewMeetingModalContext.Provider>
  )
}

export default NewMeetingModalContext
