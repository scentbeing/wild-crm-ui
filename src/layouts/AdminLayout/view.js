import React, { useState, useContext, useEffect } from 'react'
import PropTypes from 'prop-types'
import { MuiThemeProvider, createMuiTheme, withStyles } from '@material-ui/core/styles'
import CssBaseline from '@material-ui/core/CssBaseline'
import Hidden from '@material-ui/core/Hidden'
import Navigator from './Navigator'
import SpeedDial from './SpeedDial'
import MeetingDrawer from "./MeetingDrawer"

import Header from './Header'
import LayoutContext from "./context"
import MeetingDrawerContext from "./MeetingDrawer.context"
import SpeedDialContext from "./SpeedDial.context"
import { SpeedDialProvider } from './SpeedDial.context'
import { MeetingDrawerProvider } from './MeetingDrawer.context'
import TabBar from "./TabBar"
import MeetingPortalContext from "./Meeting/MeetingPortal.context"
import MeetingPortal from './Meeting/MeetingPortal'

import NewMeetingModal from "./Meeting/modals/NewMeetingModal/NewMeetingModal"
import EditMeetingModal from "./Meeting/modals/EditMeetingModal/EditMeetingModal"
import ChangeLeaderMeetingModal from "./Meeting/modals/ChangeLeaderMeetingModal/ChangeLeaderMeetingModal"
import KickUserMeetingModal from "./Meeting/modals/KickUserMeetingModal/KickUserMeetingModal"

let theme = createMuiTheme({
  spacing: 3,
  typography: {
    useNextVariants: true,
    h5: {
      fontWeight: 500,
      fontSize: 26,
      letterSpacing: 0.5,
    },
  },
  palette: {
    primary: {
      light: '#63ccff',
      main: '#009be5',
      dark: '#006db3',
    },
  },
  shape: {
    borderRadius: 8,
  },
})

theme = {
  ...theme,
  overrides: {
    MuiDrawer: {
      paper: {
        backgroundColor: '#18202c',
      },
    },
    MuiButton: {
      label: {
        textTransform: 'initial',
      },
      contained: {
        boxShadow: 'none',
        '&:active': {
          boxShadow: 'none',
        },
      },
    },
    MuiTabs: {
      root: {
        marginLeft: theme.spacing.unit,
      },
      indicator: {
        height: 3,
        borderTopLeftRadius: 3,
        borderTopRightRadius: 3,
        backgroundColor: theme.palette.common.white,
      },
    },
    MuiTab: {
      root: {
        textTransform: 'initial',
        margin: '0 16px',
        minWidth: 0,
        [theme.breakpoints.up('md')]: {
          minWidth: 0,
        },
      },
      labelContainer: {
        padding: 0,
        [theme.breakpoints.up('md')]: {
          padding: 0,
        },
      },
    },
    MuiIconButton: {
      root: {
        padding: theme.spacing.unit,
      },
    },
    MuiTooltip: {
      tooltip: {
        borderRadius: 4,
      },
    },
    MuiDivider: {
      root: {
        backgroundColor: '#404854',
      },
    },
    MuiListItemText: {
      primary: {
        fontWeight: theme.typography.fontWeightMedium,
      },
    },
    MuiListItemIcon: {
      root: {
        color: 'inherit',
        marginRight: 0,
        '& svg': {
          fontSize: 20,
        },
      },
    },
    MuiAvatar: {
      root: {
        width: 32,
        height: 32,
      },
    },
  },
  props: {
    MuiTab: {
      disableRipple: true,
    },
  },
  mixins: {
    ...theme.mixins,
    toolbar: {
      minHeight: 48,
    },
  },
}

const drawerWidth = 256

const styles = {
  root: {
    display: 'flex',
    minHeight: '100vh',
    overflow: "hidden"
  },
  body: {
    overflow: "hidden"
  },
  drawer: {
    [theme.breakpoints.up('sm')]: {
      width: drawerWidth,
      flexShrink: 0,
    },
  },
  appContent: {
    flex: 1,
    display: 'flex',
    flexDirection: 'column',
  },
  mainContent: {
    position: "relative",
    flex: 1,
    padding: '0px',
  },
}

function Layout(props) {
  console.log('props', props)
  const { location, children, classes } = props;
  const meetingPortalContext = useContext(MeetingPortalContext)
  const speedDialContext = useContext(SpeedDialContext)
  // eslint-disable-next-line no-undef
  const [mobileOpen, setMobileOpen] = useState(false)

  // useEffect(() => {
  //   if (meetingDrawerContext.isOpened && mobileOpen) {
  //     appBarWidth = drawerWidth * 2
  //     console.log('big', drawerWidth * 2, meetingDrawerContext.isOpened, mobileOpen)
  //   } else {
  //     appBarWidth = drawerWidth
  //     console.log('small', drawerWidth)
  //   }
  // }, [meetingDrawerContext, mobileOpen])


  return (
    <MuiThemeProvider theme={theme}>
      <div className={classes.root}>
        <CssBaseline />
        <nav className={classes.drawer}>
          <Hidden smUp implementation="js">
            <Navigator
              PaperProps={{ style: { width: drawerWidth } }}
              variant="temporary"
              open={mobileOpen}
              onClose={() => setMobileOpen(!mobileOpen)}
            />
          </Hidden>
          <Hidden xsDown implementation="css">
            <Navigator PaperProps={{ style: { width: drawerWidth } }} location={location} />
          </Hidden>
        </nav>
        <div className={classes.appContent}>
          <Header onDrawerToggle={() => setMobileOpen(!mobileOpen)} title={props?.pageContext?.title || ""} />
          {/* <TabBar tabNames={tabNames} /> */}
          {props?.pageContext?.tabs && (<TabBar tabs={props?.pageContext?.tabs} />)}

          <main className={classes.mainContent}>


            {children}
            {meetingPortalContext.isOpened && (
              <MeetingPortal />
            )}
          </main>

        </div>
        {/* <MeetingDrawer /> */}
      </div>
      <SpeedDial />
      <NewMeetingModal />
      <EditMeetingModal />
      <ChangeLeaderMeetingModal />
      <KickUserMeetingModal />
    </MuiThemeProvider >
  )
}

Layout.propTypes = {
  classes: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired,
  title: PropTypes.string.isRequired,
  children: PropTypes.object,
}
export default withStyles(styles)(Layout)
