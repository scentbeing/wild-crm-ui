import React, { useState } from 'react'

const SpeedDialContext = React.createContext(null);

export const SpeedDialProvider = ({ children }) => {

  const [isOpened, setIsOpened] = useState(false);
  // const [editing, toggleEditor] = useState(false);

  const value = React.useMemo(() => {
    return {
      isOpened,
      setIsOpened,
    }
  }, [isOpened]);

  return (
    <SpeedDialContext.Provider value={value}>
      {children}
    </SpeedDialContext.Provider>
  )
}

export default SpeedDialContext
