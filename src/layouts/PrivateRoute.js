import React, { useEffect } from "react"
import { navigate } from "gatsby"
import { isSignedIn } from "../features/auth/auth.store"

const PrivateRoute = ({ component: Component }) => (props) => {

  useEffect(() => {

    const main = async () => {
      const isSignedInData = await isSignedIn();

      if (!isSignedInData.result) {
        let pathParam = '';

        if (props?.location?.pathname) {
          pathParam = `?return_url=${encodeURI(props.location.pathname)}`
        }

        navigate(`/login${pathParam}`)
      }
      main();
    }
  }, [])


  console.log('props', props)
  // return <Component {...props} />
  return Component(props)
}

export default PrivateRoute
